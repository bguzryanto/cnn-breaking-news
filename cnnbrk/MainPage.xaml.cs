﻿#define DEBUG_AGENT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using cnnbrk.Resources;
using Microsoft.Phone.Tasks;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Microsoft.Phone.Net.NetworkInformation;
using RestSharp;
using Microsoft.Phone.Scheduler;

namespace cnnbrk
{
    public partial class MainPage : PhoneApplicationPage
    {
        private List<TwitterModel> tm;
        private ObservableCollection<TwitterModel> newestStories = new ObservableCollection<TwitterModel>();
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);

            


            Loaded += (s, e) =>
            {
                if (CheckInternetConnection())
                {
                    this.getStories();
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Halo!");
                    showInternetUnavailable();
                }
            };

            
            
            
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void showInternetUnavailable()
        {
            System.Diagnostics.Debug.WriteLine("internet unavailable");

            MessageBox.Show("Sorry, your internet connection is unavailable or too slow", "Information", MessageBoxButton.OK);

        }

        public static bool CheckInternetConnection()
        {
            bool isConnected = true;

            if (NetworkInterface.NetworkInterfaceType == NetworkInterfaceType.None)
            {
                isConnected = false;
            }


            return isConnected;
        }

        public void getStories()
        {
            // request new
            var cnnAPI = new CNNApi();
            var client = cnnAPI.getClient();
            var request = cnnAPI.getNewest();
            loadingIndicator.IsRunning = true;

            try
            {
                System.Diagnostics.Debug.WriteLine("Async Task started");
                var asynchandle = client.ExecuteAsync(request, response =>
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //List<TwitterModel> tm = new JsonConvert.DeserializeObject<List<TwitterModel>>(response.Content);
                        System.Diagnostics.Debug.WriteLine("Succes");
                        System.Diagnostics.Debug.WriteLine(response.Content);
                        try
                        {
                            tm = JsonConvert.DeserializeObject<List<TwitterModel>>(response.Content);
                            newestStories.Clear();
                            foreach (var item in tm)
                            {
                                newestStories.Add(item);
                                //newestStories.Add(item.text);
                                //System.Diagnostics.Debug.WriteLine(item.text);
                            }

                            listBox.ItemsSource = newestStories;
                        }
                        catch (Exception e)
                        {
                            showInternetUnavailable();
                        }

                        loadingIndicator.IsRunning = false;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine(response.ResponseStatus);
                        System.Diagnostics.Debug.WriteLine(response.StatusCode);
                        System.Diagnostics.Debug.WriteLine(response.StatusDescription);
                    }
                   
                });


            }
            catch (Exception)
            {
                MessageBox.Show("Something goes wrong");
                throw;
            }
        }

        private void ApplicationBarIconButton_Click_About(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                this.NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.RelativeOrAbsolute));
            });
        }

        private void ApplicationBarIconButton_Click_Review(object sender, EventArgs e)
        {
            MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();

            marketplaceReviewTask.Show();
        }

        private void listBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            TwitterModel twitm = (TwitterModel)listBox.SelectedItem;
            WebBrowserTask web = new WebBrowserTask();
            web.Uri = new Uri(twitm.permalink);
            web.Show();
        }

        private void ApplicationBarIconButton_Click_Refresh(object sender, EventArgs e)
        {
            tm = new List<TwitterModel>();
            listBox.ItemsSource = tm;
            this.getStories();
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}